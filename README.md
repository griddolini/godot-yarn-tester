# Godot + Yarn <3
My Godot and Yarn integration comes in three pieces.

1. **The Yarn Editor** is for creating Yarn scripts, the actual writing.
2. **The Yarn Tester** allows you to play through these scripts as if you were in a game.
3. **The Yarn Interpreter** is just the code in the background that runs your Yarn scripts in the Godot engine. You don't do anything with this, it's magic.

This guide will discuss both aspects, primarily how to write the Yarn files for use with this Godot interpreter, as well as using the Godot Yarn Tester application to its full potential.

## Where to get the Yarn Editor
- Yarn Repo: https://github.com/YarnSpinnerTool/YarnEditor
- Live Editor in Browser: https://yarnspinnertool.github.io/YarnEditor/
- Download the .exe file for Windows: https://github.com/YarnSpinnerTool/YarnEditor/releases/tag/v0.4.100

## Where to get the Yarn Tester
- If you're reading this file, you already have it.

## What do now?
So now you've got Yarn running and you want to make a game? Great! I'm not going to cover the most basic features of the Yarn Editor, but I will go over what the Yarn Interpreter is expecting, focusing primarily on logic.

## Dialogue
A single line of text in a Yarn Node is just a standard line of dialogue, which the player reads 1 line at a time. You should use this format:  
```Speaker: Speaker's lines.```  
The interpreter will detect the name of the speaker so your game can focus on them, put a speech bubble over their head, etc etc.

## Links
The most accurate description of a link is that it connects 2 nodes together. In most cases, you use links when the player needs to pick between 2 or more paths to take. However, they can also be used when you, the designer, just want to connect a node to another for the sake of structure.

Here is an example of 3 links.
#### If you write this in the Yarn file:
``` plaintext
[[What is this place? | WhereAmI]]
[[What's there to drink? | Menu]]
[[Nevermind. | MainBranch]]
```
#### The player will see:
- What is this place?  
- What's there to drink?  
- Nevermind.

So if the player clicks on the "Nevermind." option, the conversation will now move to the "MainBranch" node.

You can also use a single link to allow navigating between nodes automatically.
#### If you write this in the Yarn file:
``` plaintext
Player: Talk to you later!
[[MainBranch]]
```
#### The player will see:
a normal line of dialogue saying "Talk to you later!" and then as soon as they advance the text, they will seamlessly continue the conversation beginning in the "MainBranch" node.

## Logic
Logic has the most to cover but is where the magic happens. The basic logic syntax is always this, some stuff between double lessthan/greaterthan signs:  
```  plaintext
<< some logic >> 
```
### Custom Commands
Custom commands are mainly for the programmer or developer to worry about, and they're very simple to use. Let's say you want a dramatic lightning strike at a very specific moment in a conversation. All you need to do is this:
``` plaintext
Joe: Wow that storm sure is getting rough...
<< lightningStrike >>
Bob: Ahhhh!
```
If the player were in this conversation, the lightning strike and "Bob" screaming would happen at the exact same moment - custom commands don't stop the conversation. All this "lightningStrike" command does to your game is say "Hey, if anyone made a function called lightningStrike(), do it now!". So, it is up to the programmer and artist to make a cool lightning effect that occurs when the lightningStrike function is called.

Now lets say your programmers actually made 3 different colors of lightning strikes, and you need to specify which lightning plays at which time. You can also pass an argument to the lightningStrike function as you would expect, just by listing it:
``` plaintext
<< lightningStrike 2 >>
```
This will call `lightningStrike(2)` in the game's code.

### The set statement
The set statement allows you to assign a value to a name. Here is an example:
``` plaintext
<< set knows_secret true >>
```
This line will set a variable called "knows_secret" to a value of "true". This command runs like a custom command does, in the way that it doesn't stop the conversation. This enables you to hide dialogue and links behind some secret that the player discovers somehow. You can use this with numbers and full words as well.  

This will set the variable "gold_coins" to the value 100:
``` plaintext
<< set gold_coins 100 >>
```
However, this will ADD 100 to whatever the value of gold_coins already is (subtraction works too):
``` plaintext
<< set gold_coins +100 >>
```
You can set a variable to a word as well:
``` plaintext
<< set player_name Joanna >>
```


### The if statement
If you want something to happen under certain conditions, you need to use an **if** statement. Here are a few examples.
``` plaintext
Friend: Do you know any secrets?
<<if knows_secret is true>>
  You: Yeah, I know a secret!
  You: I won't tell anyone though!
<<endif>>
```
This node would show dialogue of your friend talking, and then it will check and see if you have previously set "knows_secret" to true. If knows_secret is false or hasn't been set yet, the player will NOT see the dialogue saying "I know a secret" and "I won't tell". 

The **endif** statement you see at the end is **REQUIRED**. You need to mark the end of what the **if** statement is checking, so the conversation can resume normally.

You can use the word "not" to invert the logic:
``` plaintext
Friend: Do you know any secrets?
<<if knows_secret is not true>>
  You: I don't know any secrets!
  You: I don't have anything to tell.
<<endif>>
```

This example shows how you can use numbers for conditional logic:
``` plaintext
Friend: How much gold do you have?
<< if gold > 100 >>
	You: I have more than 100 gold.
<< endif >>
<< if gold < 100 >>
	You: I have less than 100 gold.
<< endif >>
<< if gold == 100 >>
	You: I have exactly 100 gold.
<< endif >>
```
Here is the list of operators you can use:
``` plaintext
is : used for non numbers, true or false
not : used to invert non number logic
= : test if the values are identical
== : test if the values are identical
< : less than
> : greater than
<= : less than or equal to
>= : greater than or equal to
!= : not equal to
```

### The else statement
A lot of the time, you'll want an alternative outcome when an **if** statement fails. Instead of writing two similar **if** statements, you can simply use the **else** statement.
``` plaintext
Friend: Do you know any secrets?
<< if knows_secret is true >>
  You: Yeah, I know a secret!
  You: I won't tell anyone though!
<< else >>
  You: I don't know any secrets!
  You: I don't have anything to tell.
<< endif >>
```
Notice that the **else** is still a part of the original **if** statement, so it is also concluded by an **endif** statement.